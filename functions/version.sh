#!/usr/bin/env sh                                                               
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    version.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jerry <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/20 15:02:51 by jerry             #+#    #+#              #
#    Updated: 2020/02/20 15:08:14 by jerry            ###   ########.fr        #
#                                                                              #
#    Interpreter: GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin18)  #
#                                                                              #
#    Usage: version.sh                                                         #
#    Version: 0.0.0                                                            #
#                                                                              #
# **************************************************************************** #

version () {
	usage='version utility'
	if [ ! "$1" ]
	then
		echo "$usage"
		echo "$version"
		return
	fi
	if which "$1" 2>&- >&-
	then
		echo "$1 --version"
		"$1" --version
	else
		echo "$1 not found"
	fi
}
