# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    VBchill.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thdelmas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/27 10:26:04 by thdelmas          #+#    #+#              #
#    Updated: 2020/02/19 10:28:14 by jerry            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

VBchill() {
	if [ ! "$2" ] || [ ! -r "$2" ]
	then
		echo "Usage: VBchill VM_name /path/to/iso"
		return
	fi
	USER="${USER:-thdelmas}"
	iso_path="$2"
	vm_name="$1"
	vm_path="/sgoinfre/goinfre/Perso/$USER"
	if [ ! -d "$vm_path" ]; then
		mkdir -p "$vm_path" && chmod 700 "$vm_path"
		if [ "$?" -ne '0' ]; then
			return
		fi
	fi
	if [ ! -e "$iso_path" ] && [ -w "$vm_path" ]; then
		curl https://fai-project.org/fai-cd/faicd64-large_5.8.7.iso -o "$iso_path"
		if [ ! -e "$iso_path" ]; then
			if [ $? -eq '23' ]; then
				echo 'Bad permissions'
			fi
			echo "Iso not available"
			return
		fi
	fi
	vm_type='Debian 64'
	VBoxManage createvm --name $vm_name --ostype "$vm_type" --register
	VBoxManage modifyvm $vm_name --ioapic on
	VBoxManage modifyvm "$vm_name" --boot1 disk --boot2 dvd --boot3 none --boot4 none
# Network settings
	VBoxManage modifyvm "$vm_name" --nic1 nat
	VBoxManage modifyvm "$vm_name" --natpf1 "ssh,tcp,,2242,,22"
# Create Hardware
	vm_sda_size="$((40 * 1024))"
	vm_sda="${VMFOLDER:-/tmp}/$vm_name.vdi"
	vm_hdd_controller="SATA Controller"
	vm_dvd_controller="IDE Controller"
	vm_sdb_size="$((1 * 1024))"
	vm_ram_size="$((1 * 1024))"
	VBoxManage modifyvm "$vm_name" --memory "$vm_ram_size" --vram 128
	VBoxManage modifyvm "$vm_name" --cpus 2
	VBoxManage createhd --filename "$vm_sda" --size $vm_sda_size --format VDI --variant fixed
	VBoxManage storagectl "$vm_name" --add sata --controller IntelAHCI --name "$vm_hdd_controller"
	VBoxManage storageattach "$vm_name" --storagectl "$vm_hdd_controller" --port 0 --device 0 --type hdd --medium "$vm_sda"
# Boot
	VBoxManage storagectl "$vm_name" --add ide --controller PIIX3 --name "$vm_dvd_controller"
	VBoxManage storageattach "$vm_name" --storagectl "$vm_dvd_controller" --port 0 --device 0 --type dvddrive --medium $iso_path
	VboxManage startvm "$vm_name"
}
