# Pimp X

## How to Pimp X :

`sh -c "$(curl -fsSL 'https://gitlab.com/thdelmas/PimpX/-/raw/master/install.sh')"`

or 

`curl -fsSL 'https://gitlab.com/thdelmas/PimpX/-/raw/master/install.sh' | sh`

### Uninstall PimpX:
In your shell type the following
`pimpx_uninstall`

if it doesn't work, it probably means that you don't have pimpx installed

## Functions

Shell functions included

| Function | Purpose | Source |
|----------|---------|--------|
| pimpx_uninstall  | uninstall PimpX | [https://gitlab.com/thdelmas/PimpX/-/raw/master/functions/pimpx_uninstall](https://gitlab.com/thdelmas/PimpX/-/raw/master/functions/pimpx_uninstall) |
| take  | mkdir and cd | [https://gitlab.com/thdelmas/PimpX/-/raw/master/functions/take](https://gitlab.com/thdelmas/PimpX/-/raw/master/functions/take) |

## Scripts

Shell scriptss included

| Script | Purpose | Source |
|----------|---------|--------|
| | | |

## Alias

Pimpx's aliases
