PIMPX_VERSION=1.0.0

pimpx_hello () {
	cat <<-EOF

██████╗ ██╗███╗   ███╗██████╗     ██╗  ██╗
██╔══██╗██║████╗ ████║██╔══██╗    ╚██╗██╔╝
██████╔╝██║██╔████╔██║██████╔╝     ╚███╔╝ 
██╔═══╝ ██║██║╚██╔╝██║██╔═══╝      ██╔██╗ 
██║     ██║██║ ╚═╝ ██║██║         ██╔╝ ██╗
╚═╝     ╚═╝╚═╝     ╚═╝╚═╝         ╚═╝  ╚═╝

EOF
}

pimpx_whereis() {
	if [ "$XDG_CONFIG_HOME" -a -w "$XDG_CONFIG_HOME" -a -r "$XDG_CONFIG_HOME" ]
	then
		export PIMPX="$XDG_CONFIG_HOME/pimpx"
	elif [ "$HOME" -a -w "$HOME/.config" -a -r "$HOME/.config" ]
	then
		export PIMPX="$HOME/.config/pimpx"
	elif [ -w '/etc' -a -r '/etc' ]
	then
		export PIMPX='/etc/pimpx'
	elif [ "$HOME" -a -w "$HOME" -a -r "$HOME" ]
	then
		export PIMPX="$HOME/.pimpx"
	else
		return 1
	fi
	return 0
}

pimpx_recover() {
	if [ "$PIMPX" ]
	then
		echo "$PIMPX_VERSION" > "$PIMPX/version"
		cd "$PIMPX" && rm -rf "$PIMPX/core"
		mkdir "$PIMPX/core"
		curl https://gitlab.com/thdelmas/PimpX/-/archive/master/PimpX-master.tar.gz | gunzip | tar -x -C 'core' --strip-components=1
		echo "PimpX Updated to: $PIMPX_VERSION"
	fi
}

pimpx_update() {
	if [ ! -f "$PIMPX/version" ] || [ "$(cat "$PIMPX/version" | sed 's/\.//g')" -lt "$(echo "$PIMPX_VERSION" | sed 's/\.//g')" ]
	then
		echo "Update ? (y/n)"
		read rep
		if [ "$rep" = 'Y' -o "$rep" = 'y' -o "$rep" = 'yes' ]
		then
			echo 'Update . . .'
			pimpx_recover
		fi
	fi
}

pimpx_rc () {
	PIMPX_RC="$PIMPX/pimpx.rc"
	cat > "$PIMPX_RC" <<EOF
#!/usr/bin/env sh                                                               
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    pimpx.rc                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jerry <jerry@42.fr>                        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/07 15:44:11 by jerry             #+#    #+#              #
#    Updated: 2020/02/07 16:05:26 by jerry            ###   ########.fr        #
#                                                                              #
#    Interpreter: GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin18)  #
#                                                                              #
#    Usage: source pimpx.rc                                                    #
#    Version: 0.0.1                                                            #
#                                                                              #
# **************************************************************************** #

EOF
	echo "PIMPX='$PIMPX'" >> "$PIMPX_RC"
	echo "# ****************** Load your functions in pimpx/functions ***************** #
for i in \`ls $PIMPX/functions\`
do
	source \"$PIMPX/functions/\$i\"
done" >> "$PIMPX_RC"
	echo "# *************** Load pimpx functions in pimpx/core/functions ************** #
for i in \`ls $PIMPX/core/functions\`
do
	source \"$PIMPX/core/functions/\$i\"
done" >> "$PIMPX_RC"
	echo >> "$PIMPX_RC" 'PATH="${PATH:+$PATH:}'"$PIMPX_RC/core/scripts\""
	echo >> "$PIMPX_RC" 'PATH="${PATH:+$PATH:}'"$PIMPX_RC/scripts\""
# ! Test and source pxrc in shrc
	pimpx_write_shrc "# Test and source pimpxrc if exist
if [ -r $PIMPX_RC ]
then
	source $PIMPX_RC
fi"
}

pimpx_write_shrc () {
	case "$(echo $SHELL | rev | cut -d'/' -f1 | rev)" in
		zsh)
			PXRCFILE="$HOME/.zshrc"
			;;
		bash)
			PXRCFILE="$HOME/.bashrc"
			;;
		csh)
			PXRCFILE="$HOME/.cshrc"
			;;
		42sh)
			PXRCFILE="$HOME/.42shrc"
			;;
		*)
			PXRCFILE="$HOME/.shrc"
			;;
	esac
	if [ ! "$(cat $PXRCFILE 2>&- | grep -F 'pimpx.rc')" ]
	then
		echo "$*" >> "$PXRCFILE"
	fi
#	if [ -r "$PXRCFILE" ]
#	then
#		source "$PXRCFILE"
#	fi
}

pimpx_install() {
	pimpx_hello
	if [ ! "$PIMPX" ]
	then
		if ! pimpx_whereis
		then
			echo 'Failed to install Pimpx'
		else
			echo "Pimpx installation path: $PIMPX"
		fi
	fi
	if [ "$PIMPX" -a ! -d "$PIMPX" ]
	then
		echo 'Install . . .'
		rm -rf "$PIMPX"
		mkdir "$PIMPX"
		echo "$PIMPX_VERSION" > "$PIMPX/version"
		cd "$PIMPX"
		mkdir "$PIMPX/functions" "$PIMPX/scripts" "$PIMPX/custom" "$PIMPX/templates" "$PIMPX/log"
		rm -rf "$PIMPX/core"
		mkdir "$PIMPX/core"
		curl https://gitlab.com/thdelmas/PimpX/-/archive/master/PimpX-master.tar.gz | gunzip | tar -x -C 'core' --strip-components=1
		pimpx_rc
	elif [ "$PIMPX" -a -d "$PIMPX" ]
	then
		echo 'Pimpx is already installed and up-to-date'
		echo "PimpX version: $PIMPX_VERSION"
		pimpx_update
	else
		echo 'Pimpx not installed'
	fi
}

pimpx_install
